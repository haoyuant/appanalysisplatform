import * as cookieParser from "cookie-parser";
import * as cors from "cors";
import * as express from "express";
import * as createError from "http-errors";
import * as logger from "morgan";
import * as path from "path";
import { graphqlHttp } from "./graphqlSetup";
import { MongoHelper } from "./mongoHelper";
import { requestLoggerMiddleware } from "./request.logger.middleware";
import { appsRouter } from "./routers/appsController";

const app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(
    "/graphql",
    graphqlHttp,
);

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use(requestLoggerMiddleware);
app.use(appsRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
});

export { app };
