import * as http from "http";
import { app } from "./app";
import { MongoHelper } from "./mongoHelper";

const PORT = 8080;
const server = http.createServer(app);
server.listen(PORT);
server.on("listening", async () => {
    console.info(`Listening on port ${PORT}`);
    try {
        await MongoHelper.connect("mongodb://localhost:27017");
        console.info("Connected to localhost:27017 Mongo.");
    } catch (err) {
        console.error(err);
    }
});
