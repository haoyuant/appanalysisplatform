import * as graphqlHTTP from "express-graphql";
import { IResolvers, ITypeDefinitions } from "graphql-tools";
import * as graphqlTools from "graphql-tools";
import { MongoHelper } from "./mongoHelper";

const schemaText: ITypeDefinitions = `
    type Query {
        getReport(appKey: String!, reportKey: String!): AppReport
    }
    type Mutation {
        createReport(report: CreateReportInput!): AppReport
        finishReport(finish: FinishReportInput!): AppReport
        addAppLog(input: AddAppLogInput!): AppLog
        addExceptionLog(input: AddExceptionLogInput!): ExceptionLog
    }
    type AppReport {
        appKey: String!,
        reportKey: String!,
        version: String!,
        start: Float!,
        end: Float,
        logs: [Log],
    }
    input FinishReportInput {
        appKey: String!,
        reportKey: String!,
        end: Float!,
    }
    input CreateReportInput {
        appKey: String!,
        reportKey: String!,
        version: String!,
        start: Float!,
        description: String,
    }
    input AppReportKeyInput {
        appKey: String!,
        reportKey: String!,
    }
    input AddAppLogInput {
        keys: AppReportKeyInput!,
        appLog: AppLogInput!,
    }
    input AddExceptionLogInput {
        keys: AppReportKeyInput!,
        exceptionLog: ExceptionLogInput!,
    }
    input AppLogInput {
        time: Float!,
        type: String!,
        message: String!,
        description: String,
    }
    input ExceptionLogInput {
        appLog: AppLogInput!,
        stackTrace: String,
        lineNumber: Int,
        ColumnNumber: Int,
    }
    interface Log {
        time: Float!,
        type: String!,
        message: String!,
        description: String,
    }
    type AppLog implements Log {
        time: Float!,
        type: String!,
        message: String!,
        description: String,
    }
    type ExceptionLog implements Log {
        time: Float!,
        type: String!,
        message: String!,
        description: String,
        stackTrace: String,
        lineNumber: Int,
        ColumnNumber: Int,
    }
`;

const root: IResolvers = {
    Query: {
        getReport: async (root, {appKey, reportKey}, context) => {
            return await context.ReportCollection.findOne({appKey, reportKey});
        },
    },
    Mutation: {
        createReport: async (root, {report}, context, info) => {
            await context.ReportCollection.insertOne(report);

            return report;
        },
        finishReport: async (root, {finish}, context, info) => {
            const {appKey, reportKey, end} = finish;
            await context.ReportCollection.updateOne({appKey, reportKey}, {$set: {end}});
        },
        addAppLog: async (root, {input}, context, info) => {
            const {keys, appLog} = input;
            const {appKey, reportKey} = keys;
            const {time, type, message, description} = appLog;
            await context.ReportCollection.updateOne({appKey, reportKey}, {
                $push: {
                    logs: {time, type, message, description},
                },
            });
        },
        addExceptionLog: async (root, {input}, context, info) => {
            const {keys, exceptionLog} = input;
            const {appKey, reportKey} = keys;
            const {appLog, stackTrace, lineNumber, columnNumber} = exceptionLog;
            const {time, type, message, description} = appLog;
            await context.ReportCollection.updateOne({appKey, reportKey}, {
                $push: {
                    logs: {time, type, message, description, stackTrace, lineNumber, columnNumber},
                },
            });
        },
    },
    Log: {
        __resolveType(log) {
            if (log.type === "exception") {
                return "ExceptionLog";
            } else {
                return "AppLog";
            }
        },
    },
};

const graphqlHttp = graphqlHTTP({
    schema: graphqlTools.makeExecutableSchema({
        typeDefs: schemaText,
        resolvers: root,
    }),
    context: MongoHelper,
    graphiql: true,
});

export { graphqlHttp };
