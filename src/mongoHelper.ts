import { Collection } from "mongodb";
import * as mongodb from "mongodb";

export class MongoHelper {
    public static Client: mongodb.MongoClient;
    public static DBNAME = "local";
    public static APP_COLLECTION_NAME = "apps";
    public static REPORT_COLLECTION_NAME = "reports";
    public static AppCollection: Collection;
    public static ReportCollection: Collection;

    public static connect(url: string) {
        return new Promise(((resolve, reject) => {
            mongodb.MongoClient.connect(url, {useNewUrlParser: true}, (err, client: mongodb.MongoClient) => {
                if (err) {
                    reject(err);
                } else {
                    MongoHelper.Client = client;
                    MongoHelper.AppCollection = client.db(MongoHelper.DBNAME).collection(MongoHelper.APP_COLLECTION_NAME);
                    MongoHelper.ReportCollection = client.db(MongoHelper.DBNAME).collection(MongoHelper.REPORT_COLLECTION_NAME);
                    resolve(client);
                }
            });
        }));
    }
}
