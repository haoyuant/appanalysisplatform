import * as express from "express";
import { MongoHelper } from "../mongoHelper";

const appsRouter = express.Router();

appsRouter.get("/", async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const apps = await MongoHelper.AppCollection.find({}).toArray();
    res.render("apps", {
        apps,
    });
});

appsRouter.get("/app", async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const {appKey} = req.query;
    const app = await MongoHelper.AppCollection.findOne({appKey});
    const reports = await MongoHelper.ReportCollection.find({appKey}).toArray();
    res.render("app", {
        app,
        reports,
    });
});

appsRouter.get("/report", async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const {reportKey} = req.query;
    const report = await MongoHelper.ReportCollection.findOne({reportKey});
    res.render("report", {
        report,
    });
});

appsRouter.get("/logs", async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const {reportKey} = req.query;
    const report = await MongoHelper.ReportCollection.findOne({reportKey});
    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(report, null, 4));
});

export { appsRouter };
